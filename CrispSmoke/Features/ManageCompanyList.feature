﻿Feature: ManageCompanyList
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page
	
@SmokeTest
Scenario: Verify ManageCompany Lists
	When I click the on manage company group and lists icon 
	And I select manage company lists option
	When I Verfy the company role list 
	Then I should see the populated companies
	When I edit the populated companies on company role
	Then I should see the saved results
