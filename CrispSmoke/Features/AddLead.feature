﻿Feature: AddLead
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@SmokeTest
Scenario: Verify adding PG Lead for standard products 
When I click on the plus button 
And I select lead option to create lead
And I provide pg details on the development information page
And I provide details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see PG Lead details on dashboard page

@SmokeTest
Scenario: Verify adding PG Lead for high value products 
When I click on the plus button 
And I select lead option to create lead
And I provide pg details on the development information page
And I provide high value details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see PG Lead details on dashboard page

@SmokeTest
Scenario: Verify adding LABC Lead for standard products 	
When I click on the plus button 
And I select lead option to create lead
And I provide labc details on the development information page
And I provide details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see LABC lead details on the dashboard

@SmokeTest
Scenario: Verify adding LABC Lead for high value products 
When I click on the plus button 
And I select lead option to create lead
And I provide labc details on the development information page
And I provide high value details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see LABC lead details on the dashboard

@SmokeTest
Scenario: Verify rejecting and reinstating LABC Lead for high value products 	
When I click on the plus button 
And I select lead option to create lead
And I provide labc details on the development information page
And I provide high value details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see PG Lead details on dashboard page 
#Rejecting the Lead 
When I click on reject button on dashboard
And I enter the details for rejection on rejection dialogue window 
And I select reject option on rejection dialogue window
Then I should see rejected lead on the dashboard
#ReInstate the Lead
When I click on reinstate button on dashboard 
And I enter the notes details on reinstate dialogue window 
And I click on reinstate option on reinstate dialogue window 
Then I should see LABC lead details on the dashboard

@SmokeTest
Scenario: Verify ShortList a Lead 
When I click on the plus button 
And I select lead option to create lead
And I provide pg details on the development information page
And I provide details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see PG Lead details on dashboard page
#Then I should see lead the activity has been recorded
#Shortlisting the Lead
When I perform operation to shortlist the lead


@SmokeTest
Scenario: Verify Allocating Email To a Lead 
When I click on the plus button 
And I select lead option to create lead
And I provide pg details on the development information page
And I provide details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see PG Lead details on dashboard page
Then I should see lead the activity has been recorded
#Allocating Email To The Lead
When I perform operation email to the the lead