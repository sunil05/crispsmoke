﻿Feature: AddCompanyGroups

Background: 
    Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page


@SmokeTest
Scenario: Verify Adding Company Group
	When I click on administration button to select company groups option
	And I click on add button to create company group
	And I perform the creating company group operation 
    When I search for the above created company group 
	Then above created company Group should be displayed
	And I perform the editing company group operation 
	And I perform discontinue company group operation
	Then I should verify discontinued company group
