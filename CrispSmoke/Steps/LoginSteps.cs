﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispSmoke.Steps
{
    [Binding]
    public class LoginSteps : CrispAutomation.Support.Pages
    {
        [Given(@"I am on login page")]
        public void GivenIAmOnLoginPage()
        {
            try
            {
                WaitForElement(Loginpage.LoginButton);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Failed to Display Crisp Login Page: {0}", ex);
            }
        }
        [When(@"I enter crisp username")]
        public void WhenIEnterCrispUsername()
        {
            WaitForElement(Loginpage.Username);
            Loginpage.Username.Click();
            Loginpage.Username.Clear();
            Loginpage.Username.SendKeys(ConfigurationManager.AppSettings["CrispUsername"]);
        }
        [When(@"I enter crisp password")]
        public void WhenIEnterCrispPassword()
        {
            WaitForElement(Loginpage.Password);
            Loginpage.Password.Click();
            Loginpage.Password.Clear();
            Loginpage.Password.SendKeys(ConfigurationManager.AppSettings["CrispPassword"]);
        }
        [When(@"I click on login button")]
        public void WhenIClickOnLoginButton()
        {
            WaitForElement(Loginpage.LoginButton);
            WaitForElementToClick(Loginpage.LoginButton);
            Loginpage.LoginButton.Click();
        }
        [Then(@"I should see all items displayed on dashboard page")]
        public void ThenIShouldSeeAllItemsDisplayedOnDashboardPage()
        {
            try
            {
                WaitForElements(Dashboardpage.NavItems);
                Assert.True(Dashboardpage.NavItems.Count > 0, "User Failed to login to Crisp Application");
                foreach (var item in Dashboardpage.NavItems)
                {
                    Assert.True(item.Displayed);
                }
            }catch(Exception ex)
            {
                Console.WriteLine("User Failed to Login to Crisp: {0}", ex);
            }
        }
    }
}
