﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace CrispSmoke.Steps
{
    [Binding]
    public class SendQuoteSteps : CrispAutomation.Support.Pages
    {
        //Sending Quote

        [When(@"I click on the send quote button")]
        public void WhenIClickOnTheSendQuoteButton()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(Dashboardpage.QuoteDecisionButton);
            WaitForElementToClick(Dashboardpage.QuoteDecisionButton);
            Dashboardpage.QuoteDecisionButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(Dashboardpage.SendQuoteButton);
            WaitForElementToClick(Dashboardpage.SendQuoteButton);
            Dashboardpage.SendQuoteButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SendQuotePage.PGSendQuoteWizard);
            Assert.IsTrue(SendQuotePage.PGSendQuoteWizard.Displayed, "Failed to display Send Quote Wizard");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            if (SendQuotePage.InsuranceAgreementDiv.Count > 0)
            {
                SendQuotePage.OkButton.Click();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
        }


        //Sending Quote
        [When(@"I click on sendquote button")]
        public void WhenIClickOnSendquoteButton()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(Dashboardpage.QuoteDecisionButton);
            WaitForElementToClick(Dashboardpage.QuoteDecisionButton);
            Dashboardpage.QuoteDecisionButton.Click();
            Thread.Sleep(500);
            Assert.IsTrue(Dashboardpage.SendQuoteButton.Enabled, "Send Quote button is not enabled to Send a Quote");
            if (Dashboardpage.SendQuoteButton.Enabled)
            {
                WaitForElement(Dashboardpage.SendQuoteButton);
                Thread.Sleep(500);
                try
                {
                    Dashboardpage.SendQuoteButton.Click();
                }
                catch (Exception)
                {
                    Exception e;
                }
                Thread.Sleep(500);
            }
            else
            {
                Assert.IsFalse(Dashboardpage.SendQuoteButton.Enabled, "Failed Send a Quote because send quote button is disabled");
            }

            WaitForElement(SendQuotePage.LabcSendQuoteWizard);
            Assert.IsTrue(SendQuotePage.LabcSendQuoteWizard.Displayed, "Failed to display Send Quote Wizard");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            if (SendQuotePage.InsuranceAgreementDiv.Count > 0)
            {
                SendQuotePage.OkButton.Click();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
        }
        [When(@"I verify the details on send quote rating page")]
        public void WhenIVerifyTheDetailsOnSendQuoteRatingPage()
        {
            SendQuotePage.SendQuoteRatingPage();
        }

        [When(@"I verify the details on send quote securities page")]
        public void WhenIVerifyTheDetailsOnSendQuoteSecuritiesPage()
        {
            SendQuotePage.SendQuoteSecuritiesPage();
        }

        [When(@"I verify the details on send quote conditions page")]
        public void WhenIVerifyTheDetailsOnSendQuoteConditionsPage()
        {
            SendQuotePage.SendQuoteConditionsPage();
        }
        [When(@"I verify the details on send quote special terms page")]
        public void WhenIVerifyTheDetailsOnSendQuotespecialTermsPage()
        {
            SendQuotePage.SendQuotespecialTermsPage();
        }

        [When(@"I verify the details on send quote fees page")]
        public void WhenIVerifyTheDetailsOnSendQuoteFeesPage()
        {
            SendQuotePage.SendQuoteFeesPage();
        }

        [When(@"I verify the details on send quote file review page")]
        public void WhenIVerifyTheDetailsOnSendQuoteFileReviewPage()
        {
            SendQuotePage.SendQuoteFileReviewPage();
        }

        [When(@"I verify the details on send quote endorsements page")]
        public void WhenIVerifyTheDetailsOnSendQuoteEndorsementsPage()
        {
            SendQuotePage.SendQuoteEndorsementPage();
        }

        [When(@"I verify the details on send quote confirm details page")]
        public void WhenIVerifyTheDetailsOnSendQuoteConfirmDetailsPage()
        {
            SendQuotePage.SendQuoteConfirmDetailsPage();
        }

        [When(@"I verify the details on send quote correspondence page")]
        public void WhenIVerifyTheDetailsOnSendQuoteCorrespondencePage()
        {

            SendQuotePage.SendQuoteCorrespondencePage();
        }
        [Then(@"I click on send quote button to see quoted details on the dashboard")]
        public void ThenIClickOnSendQuoteButtonToSeeQuotedDetailsOnTheDashboard()
        {
            SendQuotePage.SendQuoteDetailsOnTheDashboard();
        }

    }
}
