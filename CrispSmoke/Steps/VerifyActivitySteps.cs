﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispSmoke.Steps
{
    [Binding]
    public class VerifyActivitySteps : CrispAutomation.Support.Pages
    {
        //Activities on lead feature steps 
        [Then(@"I should see lead the activity has been recorded")]
        public void ThenIShouldSeeTheActivityHasBeenRecorded()
        {
            ActivitiesPage.VerfiyActivities();
            Assert.IsTrue(ActivitiesPage.ActivitiesRows.Count == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New lead was manually created") && ActivitiesPage.ActivitiesList.Any(x => x.Text == "New Lead Created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New lead was manually created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "New lead was manually created") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New Lead Created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "New Lead Created - ") == 1);
            var item = ActivitiesPage.ActivitiesRows
                .FirstOrDefault(x => x.Text.Contains("New lead was manually created") && x.Text.Contains("New Lead Created - "));

            if (item != null)
            {
                item.Click();
                WaitForElement(ActivitiesPage.Priority);
                ActivitiesPage.LeadCreatedActivity();
                ActivitiesPage.PromoteLeadActivity();
                ActivitiesPage.RelegateLeadActivity();
            }
        }
        [Then(@"I should see the activities that lead has been created and shortlisted")]
        public void ThenIShouldSeeTheActivitiesThatLeadHasBeenCreatedAndShortlisted()
        {
            ActivitiesPage.VerfiyActivities();
            Assert.IsTrue(ActivitiesPage.ActivitiesRows.Count >= 2);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New lead was manually created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "New lead was manually created") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New Opportunity has been created - ") && ActivitiesPage.ActivitiesList.Any(x => x.Text == "A new sales opportunity has been created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A new sales opportunity has been created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A new sales opportunity has been created") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New Opportunity has been created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "New Opportunity has been created - ") == 1);
            var item = ActivitiesPage.ActivitiesRows
                .FirstOrDefault(x => x.Text.Contains("New Opportunity has been created - ") && x.Text.Contains("A new sales opportunity has been created"));

            if (item != null)
            {
                item.Click();
                WaitForElement(ActivitiesPage.Priority);
                ActivitiesPage.LeadShortListedActivity();
            }
        }
        //Allocating Email to the Lead Activity 

        [When(@"I perform operation email to the the lead")]
        public void WhenIPerformOperationEmailToTheTheLead()
        {
            ActivitiesPage.AllocateEmailMethod();
        }

        [Then(@"I should see the activities that email has been allocated to the lead")]
        public void ThenIShouldSeeTheActivitiesThatEmailHasBeenAllocatedToTheLead()
        {
            ActivitiesPage.VerfiyActivities();
            Assert.IsTrue(ActivitiesPage.ActivitiesRows.Count >= 2);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New lead was manually created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "New lead was manually created") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New Opportunity has been created - ") && ActivitiesPage.ActivitiesList.Any(x => x.Text == "A new sales opportunity has been created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A new sales opportunity has been created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A new sales opportunity has been created") == 1);
            //Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "SalesPg email - "));
            //Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "SalesPg email - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text.Contains("Email received from Microsoft Outlook")));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text.Contains("Email received from Microsoft Outlook")) == 1);
            var item = ActivitiesPage.ActivitiesRows
                .FirstOrDefault(x => x.Text.Contains("SalesPg email - ") && x.Text.Contains("Email received from Microsoft Outlook"));

            if (item != null)
            {
                item.Click();
                WaitForElement(ActivitiesPage.Priority);
                ActivitiesPage.LeadEmailIndexActivity();
            }
        }



        //Activities on Quote feature steps 
        [Then(@"I should see the quote submitted activity has been recorded")]
        public void ThenIShouldSeeTheQuoteSubmittedActivityHasBeenRecorded()
        {
            ActivitiesPage.VerfiyActivities();
            Assert.IsTrue(ActivitiesPage.ActivitiesRows.Count >= 2);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Quote record created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Quote record created - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quote application was created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quote application was created") == 1);
            var QuoteCReatedRow = ActivitiesPage.ActivitiesList.First(x => x.Text.Contains("Quote record created - "));
            QuoteCReatedRow.Click();
            WaitForElement(ActivitiesPage.Priority);
            ActivitiesPage.QuoteCreatedActivity();
            ActivitiesPage.PromoteQuoteActivity();
            ActivitiesPage.RelegateQuoteActivity();
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Submission of complete quote application - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Submission of complete quote application - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "The complete quote application was submitted for QGU review."));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "The complete quote application was submitted for QGU review.") == 1);
            var QuoteSubmittedRow = ActivitiesPage.ActivitiesList.First(x => x.Text.Contains("Submission of complete quote application - "));
            QuoteSubmittedRow.Click();
            WaitForElement(ActivitiesPage.Priority);
            ActivitiesPage.QuoteSubmittedActivity();
        }

        [Then(@"I should see the quote send activity has been recorded")]
        public void ThenIShouldSeeTheQuoteSendActivityHasBeenRecorded()
        {
            ActivitiesPage.VerfiyActivities();
            Assert.IsTrue(ActivitiesPage.ActivitiesRows.Count >= 4);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Quote record created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Quote record created - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quote application was created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quote application was created") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Submission of complete quote application - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Submission of complete quote application - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "The complete quote application was submitted for QGU review."));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "The complete quote application was submitted for QGU review.") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quotation was sent to the client"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quotation was sent to the client") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Send Quote - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Send Quote - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "U/W Rating Notes - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "U/W Rating Notes - ") == 1);
            var QuoteSendRow = ActivitiesPage.ActivitiesList.First(x => x.Text.Contains("Send Quote - "));
            QuoteSendRow.Click();
            WaitForElement(ActivitiesPage.Priority);
            ActivitiesPage.QuoteSentActivity();


        }

        [Then(@"I should see the quote accepted activity has been recorded")]
        public void ThenIShouldSeeTheQuoteAcceptedActivityHasBeenRecorded()
        {
            ActivitiesPage.VerfiyActivities();
            Assert.IsTrue(ActivitiesPage.ActivitiesRows.Count >= 6);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Quote record created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Quote record created - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quote application was created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quote application was created") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Submission of complete quote application - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Submission of complete quote application - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "The complete quote application was submitted for QGU review."));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "The complete quote application was submitted for QGU review.") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quotation was sent to the client"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quotation was sent to the client") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Send Quote - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Send Quote - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "U/W Rating Notes - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "U/W Rating Notes - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Accept Quote - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Accept Quote - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "The quotation was accepted by the client"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "The quotation was accepted by the client") == 2);

            var QuoteAcceptedRow = ActivitiesPage.ActivitiesList.First(x => x.Text.Contains("Accept Quote - "));
            QuoteAcceptedRow.Click();
            WaitForElement(ActivitiesPage.Priority);
            ActivitiesPage.QuoteAcceptActivity();

        }

        [Then(@"I should see the intial notice sent activity has been recorded")]
        public void ThenIShouldSeeTheIntialNoticeSentActivityHasBeenRecorded()
        {
            ActivitiesPage.VerfiyActivities();
            Assert.IsTrue(ActivitiesPage.ActivitiesRows.Count >= 7);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Quote record created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Quote record created - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quote application was created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quote application was created") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Submission of complete quote application - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Submission of complete quote application - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "The complete quote application was submitted for QGU review."));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "The complete quote application was submitted for QGU review.") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quotation was sent to the client"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quotation was sent to the client") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Send Quote - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Send Quote - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "U/W Rating Notes - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "U/W Rating Notes - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Accept Quote - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Accept Quote - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "The quotation was accepted by the client"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "The quotation was accepted by the client") == 2);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Initial Notice Issued - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text.Contains("The initial notice has been issued to the local authority.")));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Initial Notice Issued - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text.Contains("The initial notice has been issued to the local authority.")) == 1);

        }

        [Then(@"I should see the response to the intial notice activity has been recorded")]
        public void ThenIShouldSeeTheResponseToTheIntialNoticeActivityHasBeenRecorded()
        {
            ActivitiesPage.VerfiyActivities();
            Assert.IsTrue(ActivitiesPage.ActivitiesRows.Count >= 8);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Quote record created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Quote record created - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quote application was created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quote application was created") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Submission of complete quote application - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Submission of complete quote application - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "The complete quote application was submitted for QGU review."));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "The complete quote application was submitted for QGU review.") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quotation was sent to the client"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quotation was sent to the client") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Send Quote - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Send Quote - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "U/W Rating Notes - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "U/W Rating Notes - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Accept Quote - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Accept Quote - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "The quotation was accepted by the client"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "The quotation was accepted by the client") == 2);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Initial Notice Issued - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text.Contains("The initial notice has been issued to the local authority.")));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Initial Notice Issued - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text.Contains("The initial notice has been issued to the local authority.")) == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Initial Notice Response has been recorded - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text.Contains("The Initial Notice response from the local authority has been recorded")));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Initial Notice Response has been recorded - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text.Contains("The Initial Notice response from the local authority has been recorded")) == 1);
        }

        [Then(@"I should see the final notice sent activity has been recorded")]
        public void ThenIShouldSeeTheFinalNoticeSentActivityHasBeenRecorded()
        {
            ActivitiesPage.VerfiyActivities();
            Assert.IsTrue(ActivitiesPage.ActivitiesRows.Count >= 9);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Quote record created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Quote record created - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quote application was created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quote application was created") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Submission of complete quote application - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Submission of complete quote application - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "The complete quote application was submitted for QGU review."));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "The complete quote application was submitted for QGU review.") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quotation was sent to the client"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quotation was sent to the client") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Send Quote - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Send Quote - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "U/W Rating Notes - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "U/W Rating Notes - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Accept Quote - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Accept Quote - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "The quotation was accepted by the client"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "The quotation was accepted by the client") == 2);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Initial Notice Issued - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text.Contains("The initial notice has been issued to the local authority.")));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Initial Notice Issued - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text.Contains("The initial notice has been issued to the local authority.")) == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Initial Notice Response has been recorded - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text.Contains("The Initial Notice response from the local authority has been recorded")));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Initial Notice Response has been recorded - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text.Contains("The Initial Notice response from the local authority has been recorded")) == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Final Notice issued - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "The Final Notice has been issued to the local authority"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Final Notice issued - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "The Final Notice has been issued to the local authority") == 1);
        }
    }
}
